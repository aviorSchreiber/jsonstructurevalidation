import json
import operator

class ErrorGrouping:
	def __init__(self,errors):
		self.errors=errors

	#@classmethod
	def groupIt(self,toJson=False):
		errors={}
		#print("errors:{}".format(self.errors))
		for file in self.errors:
			#print(file)
			for error in file['errors_return']:
		 		if error['errorReason'] in errors.keys():
		 			errors[error['errorReason']]+=1
		 		else:
		 			errors[error['errorReason']]=1
		results=[]
		for error in errors:
			s={
				"error":error,
				"frequency":errors[error]
				}
			results.append(s)
		results.sort(key=operator.itemgetter('frequency'),reverse=True)
		self.groupErrors=results
		if toJson:
			return json.dumps(results)
		return results