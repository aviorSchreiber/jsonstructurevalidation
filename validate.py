from functions import *
import sys
import argparse
from Objects.progress.bar import Bar
from Objects.ErrorsGrouping import *
import random
from enum import Enum as enum

parser = argparse.ArgumentParser(add_help=True)
parser.add_argument('-o', '--output',dest='outputFile')
parser.add_argument('-D', '--dir',dest='directory',help="The path for directory to check all json files")
#parser.add_argument('-v', dest='verbose', action='store_true')
parser.add_argument('-l', '--limit',dest='limit',help="Limit how many files to check in the directory (Default: 500)",default=500,type=int)
parser.add_argument('-s', '--scheamaFile',dest='schemaFile',help="The Schema File to check from",required=True)
parser.add_argument('-d', '--destFile',dest='dstFile',help="Destination file to check")
parser.add_argument('-g', '--groupErrors',help="to add group errors by frequency",action='store_true',default=False)
parser.add_argument('-p', '--progress',help="Show Processing Data (available only in --dir)",action='store_true')
args = parser.parse_args()

#Custom For Stargo Project
cleanErrors=True
schemaFile=args.schemaFile
try:
	readSchemaFile=open(schemaFile,'r')
	schema=json.load(readSchemaFile)
	readSchemaFile.close()
	if "schemas" in schema:
		airSchema,seaSchema,roadSchema = parseSchema(schema)
		del airSchema['schemaVersion'] 
		del seaSchema['schemaVersion']
		del roadSchema['schemaVersion']
		multi=True
	else:
		multi=False

	if 'schemaVersion' not in schema and not multi:
		print("[Error] key schemaVersion not exist in schema file ")
		sys.exit(1)
	elif 'schemaVersion' in schema:
		del schema['schemaVersion']
except FileNotFoundError:
	print("[Error] {} file does'nt found! ".format(schemaFile))
	parser.print_help()
	sys.exit(1)	
except json.decoder.JSONDecodeError:
	errors={'success':False,'errors':'json file is not valid! ({})'.format(schemaFile)}
	print('[Error] ',errors)
	sys.exit(1)

if args.directory and not os.path.isdir(args.directory):
	print('[Error] Destination Directory is not exist')
	sys.exit(1)

print(args.directory and os.path.isdir(args.directory))
##Checking Directory Options
if args.directory and os.path.isdir(args.directory):
	#print("aaaa")

	listOfFiles=os.listdir(args.directory)
	files=[ elem for elem in listOfFiles if elem.endswith(".json")]
	#print(files)
	if args.progress:
		bar = Bar('Processing', max=len(files[:args.limit]),widgets=[Bar.eta])
		bar.fill="*"
		bar.width=64
		bar.suffix = '%(percent)d%%'
	errors=[]
	random.shuffle(files)
	for file in files[:args.limit]:
		#print(file)
		error={'filename':file}
		dstFile=args.directory+file
		try:	
			readDstFile=open(dstFile,'r')
			dstData=json.load(readDstFile)
			readDstFile.close()
		except FileNotFoundError:
			print("[Error] {} file does'nt found! ".format(dstFile))
			parser.print_help()
			sys.exit(1)
		except json.decoder.JSONDecodeError:
			error['errors_return']={'success':False,'errors':'json file is not valid! ({})'.format(dstFile)}
		if multi:
			if dstData['mode']=="AIR":
				schema=airSchema
				error['mode']="AIR"
			elif dstData['mode']=="SEA":
				schema=seaSchema
				error['mode']="SEA"
			elif dstData['mode']=="ROAD":
				schema=roadSchema
				error['mode']="ROAD"
		error['errors_return']=checkDict(schema,dstData)
		if len(error['errors_return'])>0:
			errors.append(error)
		else:
			pass
		if args.progress:
			bar.next()
	if args.progress:
		bar.finish()

##Checking One File
if args.dstFile==None and args.directory==None:
	print("[Error] Please Provide File_To_Check \nFor instance:\n./python app.py -s <Path_To_Schema_File> -d <File_To_Check>")
	parser.print_help()
	sys.exit(1)
elif args.directory==None:
	dstFile=args.dstFile
	try:	
		readDstFile=open(dstFile,'r')
		dstData=json.load(readDstFile)
		readDstFile.close()
	except FileNotFoundError:
		print("[Error] {} file does'nt found! ".format(dstFile))
		parser.print_help()
		sys.exit(1)
	except json.decoder.JSONDecodeError:
		errors={'success':False,'errors':'json file not valid! ({})'.format(dstFile)}
		if args.outputFile:
			print('[Error] ',errors)
			file=open(args.outputFile,'w')
			if cleanErrors:
				file.write(json.dumps(cleanerrors(errors)))
			else:
				file.write(json.dumps(errors))
			file.close()
		else:
			print('[Error] ',errors)
		sys.exit(1)
	if multi:
			if dstData['mode']=="AIR":
				schema=airSchema
			elif dstData['mode']=="SEA":
				schema=seaSchema
			elif dstData['mode']=="ROAD":
				schema=roadSchema
	errors=checkDict(schema,dstData)


if errors and len(errors)>0:
	if args.outputFile:
		file=open(args.outputFile,'w')
		if args.groupErrors:
			#print(errors)
			if cleanErrors:
				groupedErrors=ErrorGrouping(cleanerrors(errors))
			else:	
				groupedErrors=ErrorGrouping(errors)
			groupedErrors.groupIt()
			print(cleanErrors)
			if cleanErrors:
				file.write(json.dumps({'success':False,'errors':cleanerrors(errors),'groupedErrors':groupedErrors.groupErrors}))
			else:
				file.write(json.dumps({'success':False,'errors':errors,'groupedErrors':groupedErrors.groupErrors}))
		else:
			if cleanErrors:
				file.write(json.dumps({'success':False,'errors':cleanerrors(errors)}))
			else:
				file.write(json.dumps({'success':False,'errors':errors}))
		file.close()
	else:
		if args.groupErrors:
			groupedErrors=ErrorGrouping(cleanerrors(errors))
			groupedErrors.groupIt()
			if cleanErrors:
				print(json.dumps({'success':False,'errors':cleanerrors(errors),'groupedErrors':groupedErrors.groupErrors}))
			else:
				print(json.dumps({'success':False,'errors':errors,'groupedErrors':groupedErrors.groupErrors}))
		else:
			if cleanErrors:
				print(json.dumps({'success':False,'errors':cleanerrors(errors)}))
			else:
				print(json.dumps({'success':False,'errors':errors}))
else:
	if args.outputFile:
		file=open(args.outputFile,'w')
		file.write(json.dumps({'success':True}))
		file.close()
	else:
		print({'success':True})
