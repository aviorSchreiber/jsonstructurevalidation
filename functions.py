import json
import sys
import re
import os
from datetime import datetime
from enum import Enum as enum
condition=["",""]
conditions=[[],[]]

def buildsOrConditions(key,schema,conditions,condition):
	separatedKeys=key.split("|")
	for i in range(len(separatedKeys)):
		if i==0:
			condition[0]+="('{0}' in dstData ) or ".format(separatedKeys[i],schema[key]['type'],key)
			condition[1]+="( isinstance(dstData['{0}'],{1}) ) or ".format(separatedKeys[i],schema[key]['type'],key)
		elif i+1==len(separatedKeys):
			condition[0]+="('{0}' in dstData ) #req:schema['{2}']".format(separatedKeys[i],schema[key]['type'],key)
			condition[1]+="( isinstance(dstData['{0}'],{1}) ) #req:schema['{2}']".format(separatedKeys[i],schema[key]['type'],key)
		else:
			condition[0]+="('{0}' in dstData ) or ".format(separatedKeys[i],schema[key]['type'],key)
			condition[1]+="( isinstance(dstData['{0}'],{1}) ) or ".format(separatedKeys[i],schema[key]['type'],key)
	conditions[1].append(condition[1])
	conditions[0].append(condition[0])
	return condition,conditions

def checkListLength(ops,requiredLength,listObject,objectName):
	if ops== "max":
		if len(listObject)>requiredLength:
			return {'success':False,'errorReason':'list required maximum length of {0} in {1} '.format(requiredLength,objectName)}
		else:
			return None
	if ops== "min":
		if len(listObject)<requiredLength:
			return {'success':False,'errorReason':'list required minimum length of {0} in {1} '.format(requiredLength,objectName)}
		else:
			return None		
def checkSchemaList(key,schema,dstData,objectName):
	i=0
	schemaFieldType=schema['fieldTypes']['type']
	errors=[]
	for field in dstData:
		if not isinstance(field,eval(schemaFieldType)):
			errors.append({'success':False,'errorReason':'Object {0} not instance of \'{1}\' '.format(objectName,schema['fieldTypes']['type'])})
		else:
			if 'required' in schema['fieldTypes']:

				error=checkDict(schema['fieldTypes']['required'],field,objectName+"['{}']".format(i))
				if error:
					errors.extend(error)
		i+=1
	if len(errors)==0:
		return None
	return errors

def checkLength(ops,requiredLength,Object,objectName):
	if ops== "max":
		if len(Object)>requiredLength:
			if isinstance(Object,str):
				return {'success':False,'errorReason':'string value required maximum length of {0} in {1} '.format(requiredLength,objectName)}
			elif isinstance(Object,list):
				return {'success':False,'errorReason':'list required maximum length of {0} in {1} '.format(requiredLength,objectName)}
			else:
				return {'success':False,'errorReason':'object value required maximum length of {0} in {1} '.format(requiredLength,objectName)}
		else:
			return None
	if ops== "min":
		if len(Object)<requiredLength:
			if isinstance(Object,str):
				return {'success':False,'errorReason':'string value required minimum length of {0} in {1} '.format(requiredLength,objectName)}
			elif isinstance(Object,list):
				return {'success':False,'errorReason':'list required minimum length of {0} in {1} '.format(requiredLength,objectName)}
			else:
				return {'success':False,'errorReason':'object value required minimum length of {0} in {1} '.format(requiredLength,objectName)}
		else:
			return None	

def handleEnum(enumName,schema,dstData,objectName):
	enumValues=schema['enumValues']
	enumObject=enum(enumName,enumValues)
	if dstData not in enumObject.__members__:
		#print({'success':False,'errorReason':' {0} is not part of enumValues ({2})  in {1} '.format(dstData,objectName,enumValues)})
		return {'success':False,'errorReason':' {0} is not part of enumValues ({2})  in {1} '.format(dstData,objectName,enumValues)}
	else:
		return None

def checkDateClass(dateFormat,date,path,objectName):
	if isinstance(dateFormat,list):
		errorsReturn=[]
		listLength=len(dateFormat)
		for i in range(listLength):
			errorReturn=checkDateClass(dateFormat[i],date,path,objectName)
			if errorReturn:
				errorsReturn.append(errorReturn)

		if len(errorsReturn) < listLength:
			errorsReturn=None
		else:
			errorsReturn={'success':False,'errorReason':'value \'{3}\' not instance of datetime in {0}[\'{2}\'] datetime Format {1} '.format(path,dateFormat,objectName,date)}
	else:		
		try:
			date=datetime.strptime(date,dateFormat)
			return None
		except Exception as e:
			errorsReturn={'success':False,'errorReason':'value \'{3}\' not instance of datetime in {0}[\'{2}\'] datetime Format {1} '.format(path,dateFormat,objectName,date)}

	return errorsReturn


def checkDict(schema,dstData,objectName="main"):
	errors=[]
	condition=["",""]
	conditions=[[],[]]
	if objectName=='main':
		path="Object"
	else:
		path=objectName
	for key in schema:
		if "|" in key:
			condition,conditions=buildsOrConditions(key,schema,conditions,condition)
		else:
			#print(key)
			errorsReturn=[]
			if key not in dstData:
				if objectName=='main':
					errors.append({'success':False,'errorReason':'key \'{0}\' not exist in main {1}'.format(key,path)})
				else:
					errors.append({'success':False,'errorReason':'key \'{0}\' not exist in {1}'.format(key,path)})
			elif not isinstance(dstData[key],eval(schema[key]['type'])):
				#Handle Integer 
				if schema[key]['type'] == "int" and dstData[key]:
					try:
						int(dstData[key])
					except:
						errorsReturn={'success':False,'errorReason':'value \'{4}\' not instance of \'{1}\' in {2}[\'{0}\']'.format(key,schema[key]['type'],path,dstData[key])}
						#errors.append({'success':False,'errorReason':'key \'{0}\' not instance of \'{1}\' in {2}[\'{0}\']'.format(key,schema[key]['type'],path)})
						pass
				elif schema[key]['type'] == "float" and dstData[key]:
				#Handle Float/Duble 
					try:
						float(dstData[key])
					except:
						errorsReturn={'success':False,'errorReason':'value \'{4}\' not instance of \'{1}\' in {2}[\'{0}\']'.format(key,schema[key]['type'],path,dstData[key])}
						#errors.append({'success':False,'errorReason':'key \'{0}\' not instance of \'{1}\' in {2}[\'{0}\']'.format(key,schema[key]['type'],path)})
						pass
				elif schema[key]['type']== "datetime" and dstData[key]:
				#Handle date Class 
					errorsReturn=checkDateClass(schema[key]['dateFormat'],dstData[key],path,key)
				elif schema[key]['type']== 'enum' and dstData[key]:
				#Handle enum Class
					
					if 'enumValues' not in schema[key]:
						print("[SyntaxError][Schema] Missing enumValues in schemas['{}']".format(key))
						sys.exit(1)
					elif not isinstance(schema[key]['enumValues'],list):
						print("[SyntaxError][Schema] enumValues is not instance of list in schemas['{}']".format(key))
						sys.exit(1)
					else:
						errorsReturn=handleEnum(key,schema[key],dstData[key],path+"['{}']".format(key))
				if 'null' in schema[key]:
				#Handle null value 
					if schema[key]['null'] == True:
						if dstData[key] == None and not errorsReturn:
							pass
						elif errorsReturn:
							errors.append(errorsReturn)
					elif errorsReturn:
						errors.append(errorsReturn)
				elif dstData[key]==None:
					errorsReturn={'success':False,'errorReason':'value \'{3}\' not instance of \'{1}\' in {2}[\'{0}\']'.format(key,schema[key]['type'],path,dstData[key])}
					if errorsReturn:
						errors.append(errorsReturn)
			
			#Handle dict type
			elif 'required' in schema[key]:

				if isinstance(dstData[key],dict):
					errorsReturn=checkDict(schema[key]['required'],dstData[key],path+"['{}']".format(key))
					if len(errorsReturn)>0:
						errors.extend(errorsReturn)
					else:
						pass
				else:
					#ToDo:
					#SyntaxError For User Input
					pass
			else:
				pass
			if key in dstData:
				if 'min' in schema[key] and isinstance(dstData[key],list):
					errorsReturn=checkLength("min",schema[key]['min'],dstData[key],path+"['{}']".format(key))
					if errorsReturn:
						errors.append(errorsReturn)
				if 'max' in schema[key] and isinstance(dstData[key],list):
					errorsReturn=checkLength("max",schema[key]['max'],dstData[key],path+"['{}']".format(key))
					if errorsReturn:
						errors.append(errorsReturn)
				if 'fieldTypes' in schema[key] and isinstance(dstData[key],list):
					#checkSchemaList key,schema,dstData,objectName
					errorsReturn=checkSchemaList(key,schema[key],dstData[key],path+"['{0}']".format(key))
					if errorsReturn:
						errors.extend(errorsReturn)
				if 'min' in schema[key] and isinstance(dstData[key],str):
					errorsReturn=checkLength("min",schema[key]['min'],dstData[key],path+"['{0}']".format(key))
					if errorsReturn:
						errors.append(errorsReturn)
				if 'max' in schema[key] and isinstance(dstData[key],str):
					errorsReturn=checkLength("max",schema[key]['max'],dstData[key],path+"['{0}']".format(key))
					if errorsReturn:
						errors.append(errorsReturn)

			else:
				pass
			

	if len(conditions[0])==0:
	#ToDo
		#print("[Info] No Condition Found!")
		pass
	else:
		i=0
		for condition in conditions[0]:
			if eval(condition):
				splitedConditionsByOr=conditions[1][i].split(" or ")
				for orCondition in splitedConditionsByOr:
					try:
						if eval(orCondition):
							srcMatch=re.search("req:.*",condition)
							dstMatch=re.search('\([a-zA-Z]*\[\'[a-zA-Z]*\']',orCondition)

							if srcMatch and dstMatch:
								start=srcMatch.start()
								end=srcMatch.end()
								srcConObject=eval(condition[start+1+3:end])
								start=dstMatch.start()
								end=dstMatch.end()
								strDstConObject=orCondition[start+1:end]
								dstConObject=eval(strDstConObject)
								match=re.search("'[a-zA-Z]*'",strDstConObject)
								start=match.start()
								end=match.end()
								mainObject=strDstConObject[start+1:end-1]

								if 'required' in srcConObject and isinstance(dstConObject,dict):

									if isinstance(srcConObject['required'],dict):

										e=checkDict(srcConObject['required'],dstConObject,path+"['{0}']".format(mainObject))
										if len(e)>0:
											errors.extend(e)
										else:
											pass
									else:
										#ToDo:
										#SyntaxError For User Input
										pass
							else:
								print("Regex Failed str: {0}".format(con))
								pass
							i+=1
							continue
						else:
							errors.append({'success':False,'errorReason':'Type Error','Condition':'if {}'.format(conditions[i][1].split("#")[0])})
					except Exception as e:
						exc_type, exc_obj, exc_tb = sys.exc_info()
						fname = os.path.split(exc_tb.tb_frame.f_code.co_filename)[1]

						continue
			else:
				keys=re.findall("'[a-zA-Z]*'|or",condition.split("#")[0])
				keysStr=""
				for key in keys:
					keysStr+=" "+key
				errors.append({'success':False,'errorReason':'required key {1} not found!'.format(condition.split("#")[0],keysStr)})
			i+=1
	return errors

def parseSchema(schemas):
	i=0
	for schema in schemas['schemas']:
		if 'schemaVersion' not in schema:
			print("[Error] key schemaVersion not exist in schemas[{0}] file ".format(i))
			sys.exit(1)
		if "roadCurrencyCode" in schema['mainFreight']['required']['freight']['required']:
			roadSchema=schema
		elif "flightNumber" in schema['mainFreight']['required']['freight']['required']:
			airSchema=schema
		elif "motherVoyage" in schema['mainFreight']['required']['freight']['required']:
			seaSchema=schema
		i+=1
	return airSchema,seaSchema,roadSchema
def cleanerrors(errors):
	errors2=[]
	for file in errors:
		fileErrors=[]
		filename=file["filename"]
		mode=file['mode']
		#print(file)
		for error in file['errors_return']:
			if ('originDetails' not in error['errorReason'] and 'destinationDetails' not  in error['errorReason']):
				fileErrors.append(error)
		if len(fileErrors)>0:
			errors2.append({'filename':filename ,'mode':mode,'errors_return':fileErrors})
	return errors2


